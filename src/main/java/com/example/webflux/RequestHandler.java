package com.example.webflux;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class RequestHandler {

    private final ErgebnisService ergebnisService;

    public RequestHandler(ErgebnisService ergebnisService) {
        this.ergebnisService = ergebnisService;
    }

    public Mono<ServerResponse> getStreamVonErgebnissen(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(ergebnisService.getStreamVonErgebnissen(), Ergebnis.class);
    }

    public Mono<ServerResponse> getStreamVonAsyncErgebnissen(ServerRequest request) {
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(ergebnisService.getStreamVonAsycErgebnissen(), Ergebnis.class);
    }
}
