package com.example.webflux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ErgebnisseClient {

    private final Logger logger = LoggerFactory.getLogger(ErgebnisseClient.class);

    @Bean
    WebClient client() {
        return WebClient.create("http://localhost:8080/");
    }

    @Bean
    ApplicationRunner streamVonAsyncTasks(WebClient webClient) {
        return args -> {
            webClient.get()
                    .uri("asyncergebnisse")
                    .retrieve()
                    .bodyToFlux(Ergebnis.class)
                    .subscribe(data -> logger.info("client 1 " + data.toString()));
        };
    }

    //@Bean
    ApplicationRunner endloserStream(WebClient webClient) {
        return args -> {
            webClient.get()
                    .uri("streamergebnisse")
                    .retrieve()
                    .bodyToFlux(Ergebnis.class)
                    .subscribe(data -> logger.info("client 1 " + data.toString()));

            Thread.sleep(1000);

            webClient.get()
                    .uri("streamergebnisse")
                    .retrieve()
                    .bodyToFlux(Ergebnis.class)
                    .subscribe(data -> logger.info("client 2 " + data.toString()));

            Thread.sleep(1000);

            webClient.get()
                    .uri("streamergebnisse")
                    .retrieve()
                    .bodyToFlux(Ergebnis.class)
                    .subscribe(data -> logger.info("client 3 " + data.toString()));
        };
    }
}
