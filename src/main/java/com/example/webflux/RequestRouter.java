package com.example.webflux;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.*;

@Configuration
public class RequestRouter {

    @Bean
    RouterFunction<?> routes(RequestHandler requestHandler) {
        return route(GET("/streamergebnisse"), requestHandler::getStreamVonErgebnissen)
                .and(
                        route(GET("/asyncergebnisse"), requestHandler::getStreamVonAsyncErgebnissen))
                ;
    }

}
