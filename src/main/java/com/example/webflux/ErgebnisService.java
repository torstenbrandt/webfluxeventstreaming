package com.example.webflux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.time.LocalDateTime;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static java.time.Duration.*;
import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
public class ErgebnisService {

    private final Logger logger = LoggerFactory.getLogger(ErgebnisService.class);

    public Flux<Ergebnis> getStreamVonErgebnissen() {
        Flux<Long> interval = Flux.interval(ofSeconds(3));
        Flux<Ergebnis> events = Flux.fromStream(Stream.generate(() -> new Ergebnis("Ergebnis", LocalDateTime.now())));
        return Flux.zip(events, interval, (key, value) -> key);
    }

    public Flux<Ergebnis> getStreamVonAsycErgebnissen() {
        logger.info("Starte langlaufende Tasks");

        return Flux.create((emitter) -> {
            //Hier werden die langlaufenden Tasks gestartet
            var task1 = supplyAsync(() -> createErgebnisMitDelay("Task1", 1000));
            var task2 = supplyAsync(() -> createErgebnisMitDelay("Task2", 2250));
            var task3 = supplyAsync(() -> createErgebnisMitDelay("Task3", 3500));
            // Wenn einer der Tasks fertig ist, wird sein Ergebis in den Flux gepusht
            task1.whenComplete(emittiereErgebnis(emitter));
            task2.whenComplete(emittiereErgebnis(emitter));
            task3.whenComplete(emittiereErgebnis(emitter));
            // Wenn alle fertig sind, wird der FLux beendet
            allOf(task1, task2, task3).thenRunAsync(beendeEmitter(emitter));
        });
    }

    private BiConsumer<Ergebnis, Throwable> emittiereErgebnis(FluxSink<Ergebnis> emitter) {
        return (ergebnis, exception) -> {
            //keine Betrachtung von Exceptions
            logger.info("Emittiere Ergebnis " + ergebnis);
            emitter.next(ergebnis);
        };
    }

    private Runnable beendeEmitter(FluxSink<Ergebnis> emitter) {
        return () -> {
            // Das geht sicher schöner, aber war gerade nicht der Fokus. Sleep, damit das Senden des letzten Ergebnisses
            // und das Beenden des Flux nicht konkurrieren
            SilentSleeper.sleep(200);
            logger.info("Beende Flux");
            emitter.complete();
        };
    }

    private Ergebnis createErgebnisMitDelay(String name, int delay) {
        SilentSleeper.sleep(delay);
        return new Ergebnis(name, LocalDateTime.now());
    }

    private static class SilentSleeper {
        public static void sleep(int millis) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
