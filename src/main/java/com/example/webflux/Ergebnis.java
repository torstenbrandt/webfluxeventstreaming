package com.example.webflux;

import java.time.LocalDate;
import java.time.LocalDateTime;

public record Ergebnis(String name, LocalDateTime dateTime) {
}

